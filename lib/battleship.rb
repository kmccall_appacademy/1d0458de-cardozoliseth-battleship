require_relative 'player'
require_relative 'board'

class BattleshipGame
  attr_reader :board, :player

  def initialize(player = HumanPlayer.new("liz"), board = Board.random)
    @board = board
    @player = player
    @hit = false
  end

  def attack(position)
    board[position] == :s ?  @hit = true : @hit = false
    board[position] = :x
  end

  def count
    board.count
  end

  def play
    play_turn until game_over?
    puts "Congratz you won!!!"
  end

  def hit?
    @hit
  end

  def game_over?
    board.won?
  end

  def play_turn
    position = nil
    until valid_move?(position)
      display_status
      position = player.get_play
    end
    attack(position)
  end

  def display_status
    system("clear")
    board.display
    puts "It's a hit!" if hit?
    puts "There are #{count} ships remaining."
  end

  def valid_move?(position)
    position.is_a?(Array) && board.in_range(position)
  end

end
