class Board
  DISPLAY_BOARD = { nil => " ", :s => " ", :x => "x"}

  attr_reader :grid

  def self.default_grid
    Array.new(10) { Array.new(10)}
  end

  def self.random
    self.new(self.default_grid, true)
  end

  def initialize(grid = self.class.default_grid, random = false)
    @grid = grid
    randomize if random
  end

  def randomize(length = 10)
    length.times { place_random_ship }
  end

  def [](position)
    row, column = position
    grid[row][column]
  end

  def []=(position, value)
    row, column = position
    grid[row][column] = value
  end

  def count
    grid.flatten.count(:s)
  end

  def empty?(position = nil )
    position ? self[position].nil? : count == 0
  end

  def full?
    grid.flatten.none?(&:nil?)
  end

  def board_display
    top = (0..9).to_a.join("  ")
    puts "#{top}"
    grid.each_with_index { |row, idx| display_row(row, idx) }
  end

  def display_row(row, idx)
    chars = row.map { |value| DISPLAY_BOARD[value] }.join(" ")
    p"#{idx} #{chars}"
  end

  def place_random_ship
    raise "board is full" if full?
    position = random_position
    position = random_position until empty?(position)
    self[position] = :s
  end

  def random_position
    [rand(grid.length), rand(grid.length)]
  end

  def won?
    grid.flatten.all? { |el| el != :s }
  end

  def in_range(position)
    position.all? { |char| char.between?(0, grid.length - 1) }
  end
end
